package zkspringjpa.test.services;

public class Contributor {
	private  String name;
	private String description;
	private double price;
	
	public Contributor(String name, String description, double price) {
		setName(name);
		setDescription(description);
		setPrice(price);
		}
	public Contributor() {
		setName("");
		setDescription("");
		setPrice(11.0);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
}
