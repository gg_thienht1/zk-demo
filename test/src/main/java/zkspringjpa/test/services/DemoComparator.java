package zkspringjpa.test.services;

import java.io.Serializable;
import java.util.Comparator;

import org.jfree.ui.about.Contributor;

import zkspringjpa.test.entity.Book;

public class DemoComparator implements Comparator<Object>,Serializable {
	private static final long serialVersionUID = -2127053833562854322L;
    
	private boolean asc = true;
	private int type =0;
	
	public DemoComparator(boolean asc, int type) {
		super();
		this.asc = asc;
		this.type = type;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	public int compare(Object o1, Object o2) {
		Book b1 = (Book) o1;
		Book b2 = (Book) o2;
		
		switch (type) {
		
		case 1:
			System.out.println("đây là aaaaaaaaaaaaa: " + b1.getName().compareTo(b2.getName()) * (asc ? 1 :-1));
			return b1.getName().compareTo(b2.getName()) * (asc ? 1 :-1);
			
		case 2: return b1.getDesciption().compareTo(b2.getName()) * (asc ? 1:-1);
		case 3: return b1.getId().compareTo(b2.getId()) * (asc ? 1:-1);
		}
		return 0;
	}
//	
//	public static void main(String[] args) {
//		Book a = new Book(1, "a", desciption, price)
//		System.out.println();
//	}
}
