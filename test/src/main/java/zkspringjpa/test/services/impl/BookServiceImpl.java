package zkspringjpa.test.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.zkoss.zul.ListModelList;

import zkspringjpa.test.entity.Book;
import zkspringjpa.test.services.BookServices;

@Service("bookService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BookServiceImpl implements BookServices {

	@Autowired
	BookDao bookDao;
	public List<Book> listAll() {
		return bookDao.listAll();
	}

	public void delete(Book book) {
		bookDao.delete(book);	
	}
	
	public List<Book> search(String keyword) {
		return bookDao.search(keyword);
	}

	public Book addBook(Book book) {
		return bookDao.save(book);
	}



	public void edit(int id) {
		 bookDao.editBook(id);
		
	}

	public void edit(Book book) {
		// TODO Auto-generated method stub
		
	}


}
