package zkspringjpa.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import zkspringjpa.test.entity.Book;
import zkspringjpa.test.services.BookServices;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class BookModel{
	
	@WireVariable
	private BookServices bookService;
	private List<Book> listBookModel;
	private String keyword;
	private Book book;
	private Book newBook;
	private List<Book> list;

	
	@Init
	public void init() {
		list = new  ArrayList<Book>();
		list = bookService.listAll();
		List<Book> listBooks = bookService.listAll();
		listBookModel = new ArrayList<Book>(listBooks);
		newBook = new Book();
	}
		
	public List<Book> getList() {
		return list;
	}


	public void setList(List<Book> list) {
		this.list = list;
	}


	public List<Book> getListBookModel() {
		List<Book> listBooks = bookService.listAll();
		listBookModel = new ArrayList<Book>(listBooks);
		System.out.println("littttttttttt");
		return listBookModel;
	}


	public void setListBookModel(ArrayList<Book> listBookModel) {
		this.listBookModel = listBookModel;
	}


	public BookServices getBookService() {
		return bookService;
	}

	public void setBookService(BookServices bookService) {
		this.bookService = bookService;
	}


	public List<Book> getListBook() {
		return listBookModel;
	}


	public void setListBook(ArrayList<Book> listBook) {
		this.listBookModel = listBook;
	}


	@Command
	@NotifyChange("listBookModel")
	public void deleteBook(@BindingParam("delBook") Book book) {
		bookService.delete(book);
		listBookModel.remove(book);
	}
	@Command
	public void addBook() {
		bookService.addBook(newBook);
		listBookModel.add(newBook);
		newBook = new Book();
	}

	@Command
	@NotifyChange("listBookModel")
	public void search() {
		if(keyword==null) {
			list = bookService.listAll();
		}
		else
		{
			listBookModel=bookService.search(keyword);
		}
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	

	public Book getNewBook() {
		return newBook;
	}

	public void setNewBook(Book newBook) {
		this.newBook = newBook;
	}

	@Command
	public void showAdd(@BindingParam("bookz") Book b,@BindingParam("vm") Object vms) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("vm", vms);
		
		if(b!=null) {
			map.put("book", b);
		}
		else {
			map.put("book", new Book());
		}
		Window window = (Window) Executions.createComponents("edit.zul",null,map);
		window.doModal();
	}

}
